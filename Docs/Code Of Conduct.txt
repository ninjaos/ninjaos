Official Code of Conduct for Ninja OS
=====================================
Scope: This document covers conduct for all persons chosing to participate in
the project either by commenting, submitting code, or becomming members of the
project. 

These rules only apply to Ninja OS development, and resources such as forums,
bug trackers run by the Ninja OS foundation. In addition, "members" will be
asked to have more reserve and patience than random contributors.

1. No Doxxing, period. This includes speculation as well. Do not bring up either
offline identities, or any other potential online identity a person may or may
not have.

2. Everyone is assumed to be of "neutral"(yes, used a MUD term) gender, and
will use the they/their gender neutral pronouns.

3. Making a guess or adressing someone as a gender, sexual orientation,
nationality, or any other meat space attribute is banned, as it is considiered
a form of doxxing.

4. All contributions, including comments and posts will be relevant to Ninja OS,
including upstream and other projects we depend on and/or utilize. There is no
offtopic. There are no shortage of general purpose chat services elsewhere.

5. To the best of our abilities, Ninja OS seeks to be a neutral party to
conflicts and causes. Usernames or handles from people thate openly associate
with groups party to conflict will not be allowed.

6. Do not bring outside trouble or conflicts to this project.

7. Do not make disparaging remarks about any race, gender, sexual orientation,
national origin, or nation.

8. Do not troll, start or escalate flame wars. Simply report them. All flamings
and trolls will be deleted, and trolls and flame warriors banned/removed from
the project. No exceptions.

9. No memes or shitposting. See rule 4, and 8. There are no shortage of outlets
for memes and shitposting. Do that somewhere else. If you are a prolific
shitposter, you will need to use a new handle when contributing to Ninja OS.

10. Do not make disparaging remarks about any other contributor, even if they
say something stupid or make subpar contributions. Stupid remarks will be
deleted, and users warned in private. It goes with do not start or escalate
flame wars. Do not make disparaging remarks about any developer anywhere, or any
bad piece of technology, unless of it is relevent to the development of
Ninja OS, and relevant to fixes or workarounds.

11. Do not voice your opinion on vi, emacs, gnome, kde, systemd, or any other
bit of software that elicits strong opinions, unless it is relevant to
development of Ninja OS.

12. Ninja OS includes tools for hacking/pen testing/network security. The
assumptions will be that all hacking will be done for ethical and whitehat
reasons.

	a. No tools without an ethical usecase will be included
	b. No promotion of unethical or blackhat tools or behavior will be
	   allowed. Content will be removed and user banned.
	c. All examples will be assumed to whitehat unless proven otherwise
	d. No Exceptions

13. Pursuiant to rules 12, and 5, "We don't traffic secrets". Ninja OS is a
publicly distributed OS, under the GPLv3, distributed to the public. Information
that is not meant for the public is not something we distribute, nor will
accept. Please do not contribute it to us.

13a. We might place a temporary hold on distribution or release of certain
material while debating if it should be released.

14. There is no "offtopic" section. If it does not contribue to Ninja OS it will
no be allowed.

15. Think before posting.

16. No recruiting or soliciting. Do not attempt to recruit anyone else involved
with the project or solicit outside services. There are enough spaces for
commericial activity. This is not one.

17. Enforcement of the rules will be done on intent, and not open to
interepretation.
