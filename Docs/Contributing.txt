Contributing to Ninja OS
========================

Ninja OS has three tiers of contributors. Contributor, Member, and then
Lead/Owner, which is me, the devninja, and I have final say on everything.
Contributors are anyone who participates at all, from making a comment to
submitting some code.

3. Administrator - If the project comes time I need a leadership council, I will
appoint one. So far, this is just the dev ninja

2. Member, is someone who is officially part of the dev team, and will have some
say in direction of both Ninja OS and related operations. I will chose who gets
to be a member. Members might be assigned roles and tasks. Members will also
judge and moderate contributions from contributors.

1. Contributor - should be anonymous, but members need to make their real or more
known identies known to the founder, which shall be kept hidden from everyone
else.

With this, I will only accept people as members who have a mature mind set,
steer free from drama, and will leave their ego at the door and put making the
best Live OS we can as the only priority for this project.

Also, please read the code of conduct. It is relatively unique, and best suits
what we are trying to do here. If for whatever reason Ninja OS cannot suit your
needs as a group or project: Remember, its Free software, as are the tools
needed to make it, fork, and mod as you see fit.

Also note, we will moderate according to quality. If either discussion or code
is not up to standards it will be deleted.

Best Practices
--------------
It is entirely best practice to create a new random user name for contributing.
It is in the code of conduct to not doxx, period, and we certainly will not comb
through your personal history, but it will not prevent others from doing so. It
is recommended to us a randomly generated handle, and not to use this handle for
anything else other than Ninja OS related activities.

Post high quality, relevant, useful content only. Keep off topic posts to a
minimum.
