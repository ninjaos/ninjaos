#!/usr/bin/env bash

make_efi_part(){
  local part_dev="${1}"
  local mount_point=$(mktemp -d) || return 2
  [ -b ${part_dev} ] || return 255
  
  # Make and Mount the partition 
  mkfs.fat ${part_dev} -n Ninja_OS_UEFI
  mount ${part_dev} ${mount_point}

  # Copy syslinux EFI files
  mkdir -p "${mount_pount}/EFI/syslinux"
  cp -r "/usr/lib/syslinux/efi64/*" "${mount_point}/EFI/syslinux"

  # Copy the config
  rm -f "${mount_point}/EFI/syslinux/*.cfg"
  cp "/boot/isolinux/extlinux.conf" "${mount_pount}/EFI/syslinux/syslinux.cfg"

  # umount partition when done.
  umount ${part_dev}

}
