#!/usr/bin/env python
'''
Set background image for slock. see --help.

needs:
python-pillow
python-screeninfo

Only PNGs are tested
'''

import sys
from PIL import Image
import argparse
from screeninfo import get_monitors

options = {
    'bg_path'   : '/usr/share/images/slock/',
    'file_ext'  : None,
}

class colors:
    '''pretty terminal colors'''
    reset     ='\033[0m'
    bold      ='\033[01m'
    red       ='\033[31m'
    cyan      ='\033[36m'
    yellow    ='\033[93m'
    lightgrey ='\033[37m'
    darkgrey  ='\033[90m'
    lightblue ='\033[94m'
    lightcyan ='\033[96m'

def message(message):
    print(colors.bold + "set_slock_image.py: " + colors.reset + message)

def exit_with_error(exit,message):
    print(colors.bold + "set_slock_image.py: " + colors.red +" ¡ERROR!: " + colors.reset + message, file=sys.stderr)
    sys.exit(exit)

def populate_screen_res_table():
    '''Return a set {}, with detected resolutions'''
    res_list = set()

    res=""
    monitors = get_monitors()

    for mon in monitors:
        res = str(mon.width) + "x" + str(mon.height)
        res_list.add(res)

    return res_list

def gen_images(res_list,in_img):
    '''Generate resized images, takes two parameters: a set() of resolutions, and an image object'''
    width  = 0
    length = 0
    outfile = ""
    
    for res in res_list:
        width,height = res.split("x")
        outfile = options['bg_path'] + res + options['file_ext']
        out_img = in_img.resize((width, height))
        out_img.save(outfile)

def main():
    '''Main Program'''

    # Screen Resolution Table uses "Name":"Resolution
    screen_res_table = {}
    res_list = set()

    # Proccess Args
    parser = argparse.ArgumentParser(description='''Install image as
    background for slock. resize the image to current screen, and copy
    it to image folder. Only tested with PNGs''')
    parser.add_argument('imagefile', type=str, help="Full path to the image file")
    args = parser.parse_args()
    
    # Get the file extension from the filename
    options['file_ext'] = args.imagefile.split(".")[-1]

    # Get screen resolutions, and populate the table.
    res_list = populate_screen_res_table()
    if len(res_list) < 1:
        exit_with_error(1,"Could not detect any monitors, there is an error somewhere, quitting...")

    # Open the background image
    try:
        img_object = Image.open(args.imagefile)
        img_object.verify()
    except:
        exit_with_error(2,"Cannot open image file: " + args.imagefile + " quitting...")

    # Checks look good, inform the user
    message("Setting image " + args.imagefile + " as lockscreen image")
    
    # Generate Background images for resolutions
    gen_images(res_list,img_object)
    
    # Cleanup
    img_object.close()

if __name__ == "__main__":
    main()
