#!/usr/bin/env python
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#
import sys
import argparse
import subprocess
import random
##  External deps. Make sure you have these installed
import netifaces
import generate_mac
from scapy.all import rdpcap

# Defaults are set here. this is overwritten by argparse command input
options = {
    'vendor_file':'/usr/share/wireshark/manuf', # Default file with VID bytes
    'address_dir':'/var/interface/',
    'method'     :'total_random',
    'vendor'     : '',
    'vid_bytes'  : '',
    'loop'       : '',
    'rescramble' : '',
    'ifaces'     : []
}

class colors:
    '''pretty terminal colors'''
    reset='\033[0m'
    bold='\033[01m'
    red='\033[31m'
    cyan='\033[36m'
    yellow='\033[93m'
    green='\033[32m'

    
def message(message):
    print(colors.bold + "eth_mac_scramble.py: " + colors.reset + message)

def submsg(message):
    print("\t" + message)

def exit_with_error(exit,message):
    print(colors.bold + "eth_mac_scramble.py: " + colors.red +" ¡ERROR!: " + colors.reset + message, file=sys.stderr)
    sys.exit(exit)

def warn(message):
    print(colors.bold + "eth_mac_scramble.py: " + colors.yellow +"¡WARN!: " + colors.reset + message, file=sys.stderr)
    return
    
def check_ifaces(iface_list):
    '''take a list of interfaces and return valid ethernet interfaces as a list, takes a list as input'''
    out_list = []
    
    # Get a list of all interfaces
    int_list = netifaces.interfaces()
    # remove the loopback
    del( int_list[ int_list.index('lo') ] )
    # check if interfaces supplied are valid
    for iface in iface_list:
        if iface in int_list:
            out_list.append(iface)
        elif iface == "lo":
            warn("Loopback can't be used, skipping!")
        else:
            warn(iface + " is not a valid interface, skipping!")
    return out_list
    
def gen_mac(action,vid_bytes=None):
    '''Generates a new MAC address, according to config'''
    gen = generate_mac.generate_mac
    mac_address = ""
    
    if options['method'] == 'total_random':
        mac_address = gen.total_random()
    elif options['method'] == 'random_vendor':
        try:
            mac_address = gen.vid_file_random(options['vendor_file'])
        except:
            exit_msg = "Could not generate MAC from random file: " + options['vendor_file']
            exit_with_error(1,exit_msg)
    elif options['method'] == 'use_vendor_name':
        try:
            mac_address = gen.vid_file_vendor(options['vendor_file'],options['vendor'])
        except KeyError:
            exit_msg = "Cannot find vendor bytes for" + options['vendor'] + " in " + options['vendor_file']
            exit_with_error(1,exit_msg)
    elif options['method'] == 'use_vendor_bytes':
        if gen.is_mac_address(vid_bytes) == False:
            raise KeyError('gen_mac: invalid vendor bytes')
        mac_address = gen.vid_provided(options['vid_bytes'])
    else:
        exit_with_error(4,options['method'] + " method not implemented!")
    
    return mac_address
    
def populate_mac_iface_table(iface_list):
    '''Populate the mac_iface_table dictionary with valid interface:MAC pairs, takes one a list of valid interfaces'''
    mac_iface_table = {}
    mac_address = ""
    mac_entry   = {}
    
    if options['loop'] == False:
        for iface in iface_list:
            mac_address = gen_mac() #TODO need action for gen_mac
            mac_entry   = {iface:mac_address}
            mac_iface_table.update(mac_entry)
    elif options['loop'] == True:
        mac_address = gen_mac() #TODO need action for gen_mac
        for iface in iface_list:
            mac_address = generate_mac.another_same_vid(mac_address)
            mac_entry   = {iface:mac_address}
            mac_iface_table.update(mac_entry)
    else:
        exit_with_error(4,"Loop neither True or False, debug please")

    return mac_iface_table

def write_mac_to_file(iface,mac_address):
    '''Save MAC address so it can be used again. Takes two arguments, interface, and MAC address'''
    try:
        outfile = open(options[address_dir] + iface,"w")
        outfile.write(mac_address)
        outfile.close()
    except:
        warn("Couldn't write to " + options[address_dir] + iface)
        
def set_mac(iface,mac_address):
    '''Apply Generated MAC address to interface with NetworkManager. Takes two arguments: interface name, and MAC address. returns 1 for failure, 0 for success'''
    
    # Set interface MAC in NetworkManager
    try:
        devtype = subprocess.check_output("nmcli -t --fields type,device device|grep " + iface, shell=True)
        conname = subprocess.check_output("nmcli -t --fields name,device connection|grep " + iface, shell=True)
    except:
        warn("No connections for " + iface + ". Nothing to do")
        return 1
    devtype = devtype.decode()
    devtype = devtype.split(":")[0]
    devtype = "\'" + devtype + "\'"
    conname = conname.decode()
    conname = conname.split(":")[0]
    conname = "\'" + conname + "\'"

    #cmdline = "nmcli device modify " + iface + " " + contype + ".cloned-mac-address " + mac_address
    cmdline = []
    cmdline.append("nmcli connection modify " + conname + " " + devtype + ".cloned-mac-address " + mac_address)
    cmdline.append("nmcli connection down " + conname)
    cmdline.append("nmcli connection up " + conname)
    try:
        for line in cmdline:
            subprocess.call(line,shell=True)
    except:
        warn("Cannot set MAC for " + iface + " using networkmanager.")
        return 1

    return 0

def gen_mac_list_from_pcap(iface_mac,packets):
    '''get usable mac addresses from pcap file. two variables, mac address from interface, which is ignored, and pcap file'''
    
    # check if iface_mac is really a MAC address
    if generate_mac.is_mac_address(iface_mac) == False:
        raise TypeError(iface_mac + " is not a mac address")
    
    bad_macs = set( {iface_mac,'ff:ff:ff:ff:ff:ff', '00:00:00:00:00:00'} )
    mac_list = set()
    
    # populate list based on mac addresses in the pcap file
    for pkt in packets:
        in_mac = str(pkt.src)
        if in_mac not in bad_macs and generate_mac.is_mac_address(in_mac):
            mac_list.add(in_mac)
        
        in_mac = str(pkt.dst)
        if in_mac not in bad_macs and generate_mac.is_mac_address(in_mac):
            mac_list.add(in_mac)
    
    return mac_list
    
def gen_mac_for_iface(iface,pcap_file=None,iface_type="wired")
    '''Generate a a MAC Address for a single interface, in a specialized manner. three options, interface, and pcap_file. If pcap_file is None which is default, packets are captured live, and last if the interface is wired or wireless(default wired)'''
    mac_list   = {}
    out_mac    = ""
    mac_choice = ""
    # Magic numbers
    num_packets = 200
    
    # get MAC address from interface
    iface_mac = netifaces.ifaddresses(iface)[17][0]['addr']
    
    if pcap_file != None:
        # read packets from file
        packets  = rdpcap(pcap_file)
    else:
        # capture packets live
        if iface_type == "wired":
            packets = sniff(count=num_packets,iface=iface)
        elif iface_type == "wireless":
            packets = sniff(count=num_packets,iface=iface) #TODO, some wireless stuff
        else
            raise TypeError("gen_mac_for_iface: iface_type needs to be wired or wireless")

    # Generate a new MAC address from same VID bytes as observed.
    mac_list   = gen_mac_list_from_pcap(iface_mac,packets)
    mac_list   = list(mac_list)
    mac_choice = random.choice(mac_list)
    out_mac    = generate_mac.another_same_vid(mac_choice)

    return out_mac

def do_reset_desktop():
    '''Restart X, and NetworkManager. Mostly for legacy compatibility. Returns 0 on success, 1 on failure'''
    cmdline = ["pkill Xorg"]
    
    try:
        subprocess.call(line,shell=True)
    except:
        return 1

    return 0

def main():
    '''Main Program'''
    valid_ifaces    = []
    mac_iface_table = {}
    iface_count     = 0
    iface_errors    = 0
    iface_success   = 0
    
    ## Step One, Parse Options    
    parser = argparse.ArgumentParser(description='''MAC Scramble:
    Generate and Apply new MAC addresses for specified interfaces with
    given patterns. Use this for more fine grain control to how your
    machine appears to the router on public networks. File-format for
    mentioned files is the same as wireshark's manuf file, which is
    the default file''')
    parser.add_argument('iface_list' ,metavar="Interfaces", type=str, nargs='+', help="List of Ethernet Interfaces to scramble")
    parser.add_argument('-r','--reset'      ,help="Restarts X, and NetworkManager when complete. Legacy option for compatibility with old mh_scramble.py",action="store_true")
    parser.add_argument('-f','--file'       ,help="File to use with -d, -v and -s. Default: /usr/share/wireshark/manuf", type=str, nargs=1, default=options['vendor_file'] )
    parser.add_argument('-l', '--loop'      ,help="Use same VID bytes for all interfaces", action="store_true")
    #-
    mac_parser_options = parser.add_argument_group("MAC Address Options",'''Method for generating new MAC addresses. File Format is is the same as wireshark's manuf file''')
    mac_parser_options_x = mac_parser_options.add_mutually_exclusive_group()
    mac_parser_options_x.add_argument('-t','--total_random' ,help="Use completely random MAC Addresses",action="store_true")
    mac_parser_options_x.add_argument('-d','--random_vendor',help="Use VIDBytes from a random vendor in the manuf file", action="store_true")
    mac_parser_options_x.add_argument('-u','--use_vendor'   ,help="Specify a vendor name for VIDbytes", metavar="vendor_name", type=str, nargs=1)
    mac_parser_options_x.add_argument('-s','--smart_vid'    ,help="Pick VID bytes intellegently(Not implemented at this time)", action="store_true")
    #-
    args = parser.parse_args()
    
    # Update defaults with command line switches.
    options['ifaces']      = args.iface_list
    options['loop']        = args.loop
    options['reset']       = args.rescramble
    options['vendor_file'] = args.file
    # Methods, pick one
    if args.total_random == True:
        options['method'] = 'total_random'
    elif args.random_vendor == True:
        options['method'] = 'random_vendor'
    elif args.smart_vid == True:
        options['method'] = 'smart_vid'
    elif args.use_vendor != None:
        options['method'] = 'use_vendor'
        options['vendor'] = " ".join(args.use_vendor)

    ## Step Two, check interfaces for validity
    valid_ifaces = check_ifaces(options['ifaces'])
    iface_count = len(valid_ifaces)
    
    ## Step Three, Populate MAC Addresses for valid interfaces
    mac_iface_table = populate_mac_iface_table(valid_ifaces)
    
    ## Step Four, Set interface MAC addresses from table, record errors
    for iface in mac_iface_table:
        iface_errors += set_mac(iface,mac_iface_table[iface])
        write_mac_to_file(iface,mac_iface_table[iface])
        
    ## Step Five, check and reset if needed
    if options['reset'] == True:
        do_reset_desktop
        
    ## Step Six, report and exit
    exit_color = colors.reset
    iface_success = iface_count - iface_errors
    if iface_errors > 0:
        exit_color = colors.red
        
    message("MAC Address Scrambling Complete. Stats:")
    submsg("Total Interfaces:\t" + str(iface_count) + "\t" + " ".join(valid_ifaces) )
    submsg("Addresses Scrambled:\t" + str(iface_success) )
    submsg("Interface Errors:\t" + exit_color + str(iface_errors) + colors.reset )

if __name__ == "__main__":
    main()
