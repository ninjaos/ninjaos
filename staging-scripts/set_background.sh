#!/usr/bin/env bash
source /usr/share/ninjaos/bashlib/liveos_colors.sh
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#  Set backround image for screen locker depending on screen resolution. Takes

# directory where backgrounds are located.
IMAGES_DIR="/usr/share/images/lockscreens"

REQ_RES="1920x1080 1440x900 1280x1024 1024x768 1366x768"
declare -A CONFIG
declare -a MonList
declare -i MonCount
declare -A MonRes_table
declare -A MonImage_table
declare -i EXIT=0

help_and_exit() {
  cat 1>&2 << EOF
${BRIGHT}set_slock_bg.sh:${NOCOLOR}
Set the background that Ninja OS will use for Slock by setting variables. It
depends on a profile in the IMAGES_DIR that has background images of various
resolutions that are named in the <W>x<H>.png format. All background images
MUST be the PNG image format.

Do to restrictions of -hardened kernel, IMG_DIR needs to

	${BRIGHT}USAGE:${NOCOLOR}
	set_slock_bg.sh [-?] | [-d] <image_name>
	
	${BRIGHT}SWITCHES:${NOCOLOR}
	-?,--help	This message
	
	-d,--directory	Set IMAGES_DIR, the base dir with all the image profiles
			to use.
	
	-l,--list	List available image themes

EOF
  exit 4
}

message(){
  echo "${BRIGHT}set_slock_bg.sh:${NOCOLOR} ${@}"
}

exit_with_error(){
  echo 1>&2 "${BRIGHT}set_slock_bg.sh:${NOCOLOR} ${BRIGHT_RED}ERROR:${NOCOLOR} ${2}"
  exit ${1}
}

populate_MonRes_table() { 
  # fill out the MonRes_table with monitor names and their resolutions
  local mon; local res
  local xrandr_string
  local -i errors=0

  xrandr_string=$(xrandr |grep ${mon})
  errors+=${?}
  for mon in ${MonList[@]};do
    # grep the output of xrandr for info on one monitor
    xrandr_string=$(xrandr |grep ${mon})
    errors+=${?}
    if [ "$res" == "primary" ];then
      res=$(cut -d " " -f 4 <<< $xrandr_string |cut -d "+" -f 1)
      res=${res##*( )}
     else
      res=$(cut -d " " -f 3 <<< $xrandr_string |cut -d "+" -f 1)
      res=${res##*( )}
    fi
    MonRes_table["${mon}"]="${res}"
  done
  return ${errors}
}

list_images(){
  # check for valid image sets, returns an array
  local valid_profiles=""
  local in_profiles=$(ls "${IMAGES_DIR}") || exit_with_error 1 "Cannot read from IMG_DIR. Perhaps try -d? see --help for more info"
  
  for profile in ${in_profiles};do
    # Check if profile dir exists
    [ ! -d "${profile}" ] && continue
    # make sure all component images exist
    for res in ${REQ_RES};do
      [ ! -f "${res}.png" ] && break 1
    done
    valid_profiles+="${res} "
  done
  if [ -z ${valid_profiles} ];then
    exit_with_error 1 "No valid image profiles!"
   else
    echo ""
    return 0
  fi
}

main() {

  IMAGE=${1}
  # Make sure image directory is valid
  [ -d "${IMAGES_DIR}" ] || exit_with_error 2 "IMG_DIR is invalid, unable to continue. Perhaps try -d? see --help for more info"
  # Get a list opf monitor names
  MonList=( $(xrandr |grep " connected"|cut -d " " -f 1) )
  MonCount=${#MonList[@]}
  
  populate_MonRes_table || exit_with_error 1 "Couldn't get resolutions of all monitors, exiting..."

}
switch_checker() {
  while [ ! -z "$1" ];do
   case "$1" in
    --help|-\?)
     help_and_exit
     ;;
    --list|-l)
     CONFIG[list]=true  
     ;;
    --directory|-d)
     IMAGES_DIR="${2}"
     shift
     ;;
    *)
     PARMS+="${1}"
     ;;
   esac
   shift
  done
}       

switch_checker "${@}"
main ${PARMS}
