#
# ~/.bash_profile
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#  our .bash_profile handles the kernel command line nox and xconfigure options,
#  and starts X on virtual terminal 2

#don't write to history file
[[ -f ~/.bashrc ]] && . ~/.bashrc


# If re-scrambling, re-add the auto-ethernet account before going back to the
# Desktop
if [ -f "/tmp/rescramble" ];then
  ETH_LIST=$(cat "/tmp/rescramble")
  declare -i i=0
  for iface in ${ETH_LIST};do
    nmcli con add type ethernet con-name Auto-Ethernet${i} ifname ${iface}
    i+=1
  done
  rm "/tmp/rescramble"
fi

# Run the desktop. first virtual terminal, TTY1 is the desktop. if the No X
# is specified on the desktop
if [[ ! $DISPLAY && $(tty) = /dev/tty1 &&  $(cat /proc/cmdline) != *nox* ]]; then
     exec startxfce4
fi

