#!/usr/bin/env bash
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# This script will enable or disable new USB conntections with usbctl. Requires
# -hardened kernel patches

ACTION=${1,,}
icon=usb
icon_error=error
icon_enabled=usbctl-enabled
icon_disabled=usbctl-disabled

declare -i EXIT=0

notify_usb_status(){
  #returns active for working, inactive for not working
  local status="$(sysctl kernel.deny_new_usb)"
  case ${status: -1} in
   0)
    notify-send "USB Enabled" "New USB devices may be plugged in" --icon=${icon_enabled}
    echo "status: enabled"
    return 0
    ;;
   1)
    notify-send "USB Disabled" "New USB devices will be ignored" --icon=${icon_disabled}
    echo "status: disabled"
    return 0
    ;;
   *)
    notify-send "USB Status: Unexpected ERROR" "Something has gone terribly wrong, please check usbctl on the command line manually" --icon=${icon-error}
    echo "status: error"
    return 1
    ;;
  esac
}

help_and_exit() {
  echo "$(tput bold)bluetoothctl:$(tput sgr0)" 1>&2
  cat 1>&2 << EOF
This script wraps enabling and disabling of USB Firewall rules for the gui,
using libnotify.

	USAGE:
/usr/share/ninjaos/scripts/usbctl.sh [enable|disable|status]

EOF
exit 1
}

case $ACTION in
 enable)
  pkexec usbctl enable
  EXIT=${?}
  if [ ${EXIT} -ne 0 ];then
    echo "Could Not Start!"
    notify-send "Enable USB" "Could not enable USB" --icon=${icon-error}
   else
    notify-send "Enabled USB" "You may plug in new USB devices" --icon=${icon_enabled}
    echo "Enabling..."
  fi
  ;;
 disable)
  pkexec usbctl disable
  EXIT=${?}
  if [ ${EXIT} -ne 0 ];then
    echo "Could Not Stop!"
    notify-send "Disable USB" "Could NOT disable USB" --icon=${icon-error}
   else
    notify-send "Disabled USB" "New USB connections will be ignored" --icon=${icon_disabled}
    echo "Disabling..."
  fi
  ;;
 status)
  notify_usb_status
  EXIT=${?}
  ;;
 *)
  help_and_exit
  ;;
esac

if [[ $EXIT -ne 0 ]];then
  echo "${0}: Script threw a code somewhere $(tput bold;tput setaf 1)!FAIL!$(tput sgr0)" 1>&2
  notify-send "USBCtl FAIL!" "Last Action failed Exit code: ${EXIT}"
fi
exit ${EXIT}
