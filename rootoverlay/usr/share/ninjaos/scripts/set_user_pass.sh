#!/usr/bin/env bash
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# Set user password, warn on fail

passwd
if [ ${?} -ne 0 ];then
  Xdialog --icon dialog-error --title "Set Password" --msgbox "Failed to Set Password!" 8 45
  exit 1
fi
exit 0
