#!/usr/bin/env bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#

# set min and max length for password. default is a 24 char password.
MIN=24
MAX=24

help_and_exit() {
cat 1>&2 << EOF
newpass: simple script to generate new passwords. The default is a min length
of 20, and max length of 30. If only one parameter is specified, a password of exact
length is used.

	Usage:
	$ newpass [min length] [max length]
EOF
exit 1
}

main() {
  [ "${1}" == "--help" ] && help_and_exit
  # If there are two parameters, the first is the minimumm, second maximum
  if [ ! -z ${2} ];then
    MIN=${1}
    MAX=${2}
  # IF there is one parameter, that is the length
   elif [ ! -z ${1} ];then
    MIN=${1}
    MAX=${1}
  fi
  makepasswd --minchars=${MIN} --maxchars=${MAX}
}
