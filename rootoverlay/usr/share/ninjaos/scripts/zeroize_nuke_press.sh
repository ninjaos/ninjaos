#!/usr/bin/env bash

# Count $X_PRESS amount of presses in $TICK seconds before activating
# self destruct. Link this script to an X11 keypress

X_PRESS=3
TICK=0.8

SCRIPT_NAME=$(basename $0)
COUNT=$(ps aux|grep ${SCRIPT_NAME}|wc -l)
COUNT=$(( ${COUNT} - 1 ))

run_zeroize(){
  pkexec cp -f /etc/shadow.orig /etc/shadow
  cp -f /usr/share/ninjaos/scripts/liveos_sd.sh ~/.bash_profile
  pkill X
}

cat 2>&1 << EOF
This script is designed to be called from a button or shortcut key press.
It will activate Self Destruct/Zerioze mode. It will only
run if you run it X_PRESS times in TICK seconds. Defaults:
	X_PRESS=3	TICK=0.8

EOF
sleep $TICK
[ "${COUNT}" -gt "${X_PRESS}" ] && run_zeroize
