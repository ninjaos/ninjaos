#!/usr/bin/env bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#	Converts disk images made with proprietary Windows(tm) tools into
# universal .ISO files.
source /usr/share/ninjaos/bashlib/liveos_colors.sh

SUPPORTED_EXT="ccd nrg daa uif mdf"
PROGRESS=false

declare -i PROG_COUNTER=100

help_and_exit() {
  cat 1>&2 << EOF
${BRIGHT}2iso.sh${NOCOLOR}:
	Converts disk images made with proprietary Windows(tm) tools into
standard ISO9660 .ISO files. Automaticly detects format based on file name.
Supports the following formats:
	.nrg - Nero Burning Rom
	.ccd - CloneCD
	.uif - Universal Image Format by MagicISO
	.daa - Direct Access Archive from PowerISO
	.mdf - Alcohol %120 images

	${BRIGHT}Usage${NOCOLOR}:
	2iso.sh [--progress|-p] <filenames>

	${BRIGHT}Options${NOCOLOR}:
	-p, --progress	Give a terse updating progress report. Usefull for
			scripts.

EOF
exit 1
}

message() {
  echo "${BRIGHT}2iso.sh${NOCOLOR}: $@"
}
exit_with_error() {
  message "${BRIGHT_RED}!ERROR!${NOCOLOR} ${2}" 1>&2
  exit $1
}
warn() {
    message "${BRIGHT_YELLOW}Warn:${NOCOLOR} ${@}" 1>&2
}

file_process() {
  # proccess one file into an iso. dynamically guess type.
  # to parameters extension then filename
  local -i exit=0
  local method="${1}"
  local infile="${@:2}"
  local file_base="${infile%.*}"
  # output file is just renamed to .iso, we delete the original later
  local outfile="${file_base}.iso"
  ${method}iso "${infile}" "${outfile}"
  exit=$?
  if [ ${exit} -ne 0 ];then
    warn "ISO converstion for ${infile} failed!"
    return 1
  fi
  #securely delete the original file
  shred --remove=wipesync -f -n2 "${infile}"
  return 0
}

switch_checker() {
  while [ ! -z "$1" ];do
   case "$1" in
    --help|-\?)
     help_and_exit
     ;;
    --progress|-p)
     PROGRESS=true
     ;;
    *)
     # This is not a switch, we add all non switch parameters to $PARAMS and
     # then when we run main(), its fed to main() as if it were the command line
     PARMS+="${1}"
     ;;
   esac
   shift
  done
}

set_prog_counter(){
  # calculates the percentage of total a single file is, and sets the counter.
  local -i count="${#}"
  PROG_COUNTER=$(( 100 / ${count} ))
}

main() {
  local -i exit=0
  local -i ext_fld=0
  local ext=""
  local filelist=("${@}")
  local -i done=0
  [ -z $1 ] && help_and_exit
  [ ${PROGRESS} == "true" ] && set_prog_counter

  for file in ${filelist[@]};do
    #check if file exists. If file does not exist go back to the top
    if [ ! -f ${file} ];then
      warn "${file} does not exist"
      exit+=1
      continue
    fi
    # check file extension:
    ext_fld=$( grep -o "\." <<< ${file} |wc -l )
    ext_fld=$(($exit_fld + 1))
    ext=$( cut -d "." -f${ext_fld} <<< "${file}" )
    ext=${ext,,}
    if [[ ${SUPPORTED_EXT} = *${ext}* ]];then
      file_process ${ext} "${file}" || exit+=1
      if [ ${PROGRESS} == "true" ];then
        done+=${counter}
        echo "${done}"
      fi
     else
      warn "${file} is not a supported file(${SUPPORTED_EXT})."
    fi
  done
  # check exit codes before we exit
  [ $exit -ne 0 ] && exit_with_error $exit "There were ${exit} errors"
  exit 0
}

switch_checker "${@}"
main "${PARMS}"
