#!/usr/bin/env bash
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# This script disguises XFCE as another Operating System. EXPERIMENTAL
# use as:
#	xfce4-camo.sh <OS-NAME>

CAMO_LIST="winxp, win7, win10, and osx"

camo_win7(){
  # Use B00merang Windows 7 theme, also reload the pannel
  #cp /usr/share/camo/win7/whiskermenu-1.rc /home/user/.config/xfce4/panel/
  #sed -i 's/.*button-icon=.*/button-icon=start-here/' /home/user/.config/xfce4/panel/whiskermenu-4.rc
  sed -i 's/.*button-title=.*/button-title=Start/' /home/user/.config/xfce4/panel/whiskermenu-4.rc
  #chown user:users /home/user/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml

  xfconf-query -c xsettings -p /Net/ThemeName -s "Windows7"
  xfconf-query -c xsettings -p /Net/IconThemeName -s "Windows-7"
  xfconf-query -c xsettings -p /Gtk/FontName "Tahoma 10"
  xfconf-query -c xfwm4 -p /general/theme -s "Redmond"
  xfconf-query -c xfwm4 -p /general/title_font -s "Tahoma Bold 9"

  # Reload
  xfce4-panel -r
}

camo_winxp(){
  # Use the Windows XP themes built into XFCE. Also reload the panel
  #cp /usr/share/camo/winxp/whiskermenu-1.rc /home/user/.config/xfce4/panel/
  #sed -i 's/.*button-icon=.*/button-icon=start-here/' /home/user/.config/xfce4/panel/whiskermenu-4.rc
  sed -i 's/.*button-title=.*/button-title=Start/' /home/user/.config/xfce4/panel/whiskermenu-4.rc
  xfconf-query -c xsettings -p /Net/ThemeName -s "Xfce-redmondxp"
  xfconf-query -c xsettings -p /Net/IconThemeName -s "windows-xp-icon-theme"
  xfconf-query -c xsettings -p /Gtk/FontName "Tahoma 10"
  xfconf-query -c xfwm4 -p /general/theme -s "RedmondXP"
  xfconf-query -c xfwm4 -p /general/title_font -s "Tahoma Bold 9"

  # Reload
  xfce4-panel -r
}

camo_win10(){
  # Use B00merang Windows 10 theme, also reload the panel
  #cp /usr/share/camo/winxp/whiskermenu-1.rc /home/user/.config/xfce4/panel/
  #sed -i 's/.*button-icon=.*/button-icon=start-here/' /home/user/.config/xfce4/panel/whiskermenu-4.rc
  sed -i 's/.*button-title=.*/button-title=Start/' /home/user/.config/xfce4/panel/whiskermenu-4.rc
  #xfconf-query -c xfce4-panel -p /plugins/plugin-1/button-icon -s "start_here"
  xfconf-query -c xsettings -p /Net/ThemeName -s "Windows10"
  xfconf-query -c xsettings -p /Net/IconThemeName -s "Windows10"
  xfconf-query -c xsettings -p /Gtk/FontName "Tahoma 10"
  xfconf-query -c xfwm4 -p /general/theme -s "Windows10"
  xfconf-query -c xfwm4 -p /general/title_font -s "Tahoma Bold 9"

  # Reload
  xfce4-panel -r
}

camo_default() {
  # Use XFCE default light theme. Also reload the panel
  #cp /usr/share/camo/winxp/whiskermenu-1.rc /home/user/.config/xfce4/panel/
  #sed -i 's/.*button-icon=.*/button-icon=start-here/' /home/user/.config/xfce4/panel/whiskermenu-4.rc
  sed -i 's/.*button-title=.*/button-title=Applications/' /home/user/.config/xfce4/panel/whiskermenu-4.rc
  #xfconf-query -c xfce4-panel -p /plugins/plugin-1/button-icon -s "start_icon8"
  xfconf-query -c xsettings -p /Net/ThemeName -s "Windows10"
  xfconf-query -c xsettings -p /Net/IconThemeName -s "Adwaita"
  xfconf-query -c xsettings -p /Gtk/FontName "Sans 10"
  xfconf-query -c xfwm4 -p /general/theme -s "Default"
  xfconf-query -c xfwm4 -p /general/title_font -s "Sans Bold 9"
  
  # Reload
  xfce4-panel -r
}

case ${1} in
  winxp)
    #Work in Proggress. Please note this does not work entirely as expected.
    camo_winxp
    echo "Windows XP Camouflage Work in Progress"
    ;;
  win7)
    camo_win7
    echo "Windows 7 Camoflage Work in Progress"
    ;;
  win10)
    #xfconf-query -c xfce4-panel -p /plugins/plugin-1/button-icon -s "start_icon8"
    camo_win10
    echo "Windows 10 Camouflage Work in Progress"
    exit 1
    ;;
  osx)
    echo "Mac OSX Camouflage not implemented yet"
    exit 1
    ;;
  *)
    echo "We don't have camouflauge for $1, valid options are \"${CAMO_LIST}\""
    exit 1
    ;;
esac

