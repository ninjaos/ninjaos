#!/usr/bin/env bash
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# Set wallpaper on all attached screens on XFCE. Modern versions have settings
# per screen, per card, which makes configurating for a Live OS tough.

WALLPAPER=/usr/share/images/wallpaper.jpg


declare -i EXIT_CODE=0
#Get a list and count of the number of attached monitors
MonList=( $(xrandr |grep " connected"|cut -d " " -f 1) )
MonCount=${#MonList[@]}

SCREEN_LIST=$(xfconf-query -c xfce4-desktop -p /backdrop -l|egrep -e "screen.*/monitor.*image-path$" -e "screen.*/monitor.*/last-image$")

for screen in ${SCREEN_LIST}; do
  xfconf-query -c xfce4-desktop -p ${screen} -n -t string -s ${WALLPAPER}
  EXIT_CODE+=${?}
  xfconf-query -c xfce4-desktop -p ${screen} -s ${WALLPAPER}
  EXIT_CODE+=${?}
done

exit ${EXIT_CODE}
