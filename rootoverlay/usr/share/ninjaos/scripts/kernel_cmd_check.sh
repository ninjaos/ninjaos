#!/usr/bin/env bash
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# This script proccesses Ninja OS specific items entered on the kernel command
# line. It is called by systemd at startup. options "nox" and "xconfigure" are
# in ~/.bash_profile
source /usr/share/ninjaos/bashlib/liveos_boot.sh
USERHOME="/home/user"
USERNAME="user"
USERGROUP="users"

zeroize() {
  rm "${USERHOME}/.bash_profile"
  cp /usr/share/ninjaos/scripts/liveos_sd.sh "${USERHOME}/.bash_profile"
  exit
}

auto_dpi() {
  cp /usr/share/ninjaos/scripts/auto_dpi.desktop "${USERHOME}/.config/autostart/"
  chown ${USERNAME}:${USERGROUP} "${USERHOME}/.config/autostart/auto_dpi.desktop"

  echo AUTO_DPI="true" >> /etc/environment
}

priv_mode() {
  ## activate privacy mode
  # scramble ethernet MACs and the system hostname
  /usr/share/ninjaos/scripts/mh_scramble.py

  # Add "rescramble" option to the desktop
  cp /usr/share/ninjaos/scripts/notify-privmode.desktop "${USERHOME}/.config/autostart/"
  if [ ! -d "${USERHOME}/Desktop/" ] ;then
    mkdir "${USERHOME}/Desktop/"
    chown ${USERNAME}:${USERGROUP} "${USERHOME}/Desktop/"
  fi
  cp /usr/share/ninjaos/scripts/rescramble.desktop "${USERHOME}/Desktop/"
  cp /usr/share/ninjaos/scripts/rescramble.desktop "/usr/share/applications/"
  chown ${USERNAME}:${USERGROUP} "${USERHOME}/.config/autostart/notify-privmode.desktop"
  chown ${USERNAME}:${USERGROUP} "${USERHOME}/Desktop/rescramble.desktop"
  chmod +x "${USERHOME}/Desktop/rescramble.desktop"
  
  echo PRIV_MODE="true" >> /etc/environment

}

start_sshd() {
  #generate ssh keys and start sshd.
  /usr/share/ninjaos/scripts/gen_ssh_keys.sh
  systemctl start sshd
  echo START_SSHD="true" >> /etc/environment
}

x_configure() {
  # generate a configuration before starting X, useful for troubleshooting or
  # faulty hw
  sudo Xorg -configure
  sudo mv -f /root/xorg.conf.new /etc/X11/xorg.conf
  echo X_CONF="true" >> /etc/environment
}

cmdline_check(){
    # check for options in /proc/cmdline
    set $CMDLINE
    while [ ! -z "$1" ];do
        case "$1" in
          selfdestruct|zeroize|zzz)
            zeroize
            ;;
          privmodetrue)
            priv_mode
            ;;
          autodpi)
            auto_dpi
            ;;
          sshd)
            start_sshd
            ;;
          xconfigure)
            x_configure
            ;;
          camo-*)
            local CAMO_PATTERN=$(cut -f 2 -d "-" <<< $1 )
            /usr/share/ninjaos/scripts/xfce4-camo.sh $CAMO_PATTERN
            ;;
          *)
            #catch all for words we don't care about
            true
            ;;
        esac
        shift
    done   
}

cmdline_check "${@}"
