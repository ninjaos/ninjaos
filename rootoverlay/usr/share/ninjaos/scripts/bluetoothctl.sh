#!/bin/bash
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# This script will enable or disable bluetooth rules. Disabled by default
#case insensative $1
ACTION=${1,,}
icon=blueman
icon_error=bluetooth-error
declare -i EXIT=0

notify_bluetooth_status(){
  #returns active for working, inactive for not working
  local status="$(systemctl is-active bluetooth)"
  case $status in
   active)
    notify-send "Bluetooth Status: Running" "Bluetooth subsystem loaded!" --icon=${icon}
    echo "status: enabled"
    return 0
    ;;
   inactive)
    notify-send "Bluetooth Status: Stopped" "Bluetooth subsystem halted!" --icon=${icon}
    echo "status: disabled"
    return 0
    ;;
   unknown)
    notify-send "Bluetooth Status: ERROR" "Cannot find bluetooth unit!" --icon=${icon-error}
    echo "status: unit_error"
    return 1
    ;;
   *)
    notify-send "Bluetooth Status: Unexpected ERROR" "Something has gone terribly wrong, please check bluetooth and systemctl manually" --icon=${icon-error}
    echo "status: systemd_error"
    return 1
    ;;
  esac
}

help_and_exit() {
  echo "$(tput bold)bluetoothctl:$(tput sgr0)" 1>&2
  cat 1>&2 << EOF
This script wraps enabling and disabling of bluetooth subsystem

	USAGE:
/usr/share/ninjaos/scripts/bluetoothctl.sh [start|stop|status]

EOF
exit 1
}

case $ACTION in
 start)
  pkexec systemctl start bluetooth
  EXIT=${?}
  if [ ${EXIT} -ne 0 ];then
    echo "Could Not Start!"
    notify-send "Starting Bluetooth" "Bluetooth could not start!" --icon=${icon-error}
   else
    notify-send "Starting Bluetooth" "Loading Bluetooth Subsystem" --icon=${icon}
    echo "Starting..."
  fi
  ;;
 stop)
  pkexec systemctl stop bluetooth
  EXIT=${?}
  if [ ${EXIT} -ne 0 ];then
    echo "Could Not Stop!"
    notify-send "Stopping Bluetooth" "Bluetooth could not stop!" --icon=${icon-error}
   else
    notify-send "Stopping Bluetooth" "Stopping Bluetooth Subsystem" --icon=${icon}
    echo "stopping..."
  fi
  ;;
 status)
  notify_bluetooth_status
  EXIT=${?}
  ;;
 *)
  help_and_exit
  ;;
esac

if [[ $EXIT -ne 0 ]];then
  echo "${0}: Script threw a code somewhere $(tput bold;tput setaf 1)!FAIL!$(tput sgr0)" 1>&2
  notify-send "Bluetooth Control FAIL!" "Last Action failed Exit code: ${EXIT}"
fi
exit ${EXIT}
