#!/usr/bin/env bash
source /usr/share/ninjaos/bashlib/liveos_boot.sh
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# This script runs at start up, stays resident and watches for the OS drive to
# be unplugged. If so it shuts the system down.

TICK=".3333"

sysrq_reboot() {
  echo 1 > /proc/sys/kernel/sysrq 
  echo b > /proc/sysrq-trigger
}

tamper_reboot() {
  # This function reboots the machine if tampering is found with any of
  # components. We try a few shutdown methods until one sticks
  while true;do
    sysrq_reboot &
    /var/emergency_bin/busybox reboot -f &
    /tmp/emergency_bin/busybox reboot -f &
  done
}

tamper_check() {
  # This function checks if any of the binaries needed for emergency actions
  # are tampered with. busybox is needed for this script, and pv is needed for
  # zeroize.
  [ -f /tmp/emergency_bin/busybox ] || tamper_reboot
  #[ -f /var/emergency_bin/pv ] || tamper_reboot
}
shutdown_check() {
  # If this script is killed by shutdown, regardless, it will reboot the system
  # Therefor the shutdown command will reboot. The solution is to check for
  # shutdown status before checking for tampering.
  
  # If Zeroize/SD is going on, skip this check
  [ -f /tmp/zeroize ] && return
  
  local lines_reboot=$(systemctl list-jobs reboot.target|wc -l)
  local lines_poweroff=$(systemctl list-jobs shutdown.target|wc -l)

  [ "${lines_reboot}" -gt "1" ] && reboot -f
  [ "${lines_poweroff}" -gt "1" ] && poweroff -f
}

tamper_kill(){
  # This function runs if we get an interreupt, i.e. someone tries to kill
  # poor mr tamper check.
  shutdown_check
  tamper_reboot
}

# If someone tries to disrupt the script while running, reboot.
main(){
  trap "tamper_kill" 2 3 5 6 9 15
  trap "true" 1
  # adjusting the priority of this script so it doesn't get interrupted.
  renice -20 $$ &> /dev/null

  while [ -b ${BOOTDEV} ];do
    # lets check if the system is shutting down, if do the correct action.
    shutdown_check
    # Every tick we check if the system has been tampered with
    tamper_check
    /tmp/emergency_bin/busybox sleep ${TICK}
  done

  # one final shutdown check in case we are killed by systemd on a shutdown.
  shutdown_check
  # reboot the system. This script 
  while true;do
    sysrq_reboot
  done
}

main "${@}"
