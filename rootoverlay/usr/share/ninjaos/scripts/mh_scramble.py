#!/usr/bin/env python3
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#   Mac & Host Scramble. This script Randomizes the system hostname and
# the MAC addresses of all detected Ethernet inferfaces. Detects interface
# type instead of guessing via naming scheme.
# This is a python re-write of the original BASH script
#
# needs python-netifaces

import random
import argparse
import subprocess
import sys
import os
import glob

#check if netifaces is installed
try:
    import netifaces
except:
    print("mh_scramble.py: python-netifaces is not installed, exiting")
    sys.exit(4)

class colors:
    '''pretty terminal colors'''
    reset='\033[0m'
    bold='\033[01m'
    red='\033[31m'
    cyan='\033[36m'
    yellow='\033[93m'
    lightgrey='\033[37m'
    darkgrey='\033[90m'
    lightblue='\033[94m'
    lightcyan='\033[96m'
    
def get_eth_interfaces():
    '''return all ethernet interfaces detected as a list'''
    #all interfaces
    int_list = netifaces.interfaces()
    #remove the loopback
    del( int_list[ int_list.index('lo') ] )
    #now make a list with only ethernet addresses
    eth_list = []
    for iface in int_list:
        if "eth" in iface:
            eth_list.append(iface)
    return eth_list
    
def bytes2string(in_bytes):
    '''takes output from subproccess.check_output() and returns it as a string'''
    output = ""
    output = str(in_bytes)
    output = output.strip("\\n\'")
    output = output.strip("b\'")
    output = output.strip()
    return output

def random_hostname():
    '''Generate a pronouncable, believable hostname, returns string'''
    try:
        new_hostname = bytes2string( subprocess.check_output(['apg', '-a0', '-n1', '-m4', '-x10', '-Mnc', '-cP34nU7w4a4S5Lt3D', '-q']) )
    except FileNotFoundError:
        exit_with_error(4,"APG Binary Not Found")
    return new_hostname

def random_machine_id():
    '''Generate a new number for /etc/machine-id, returns 32 char hexdec as a string'''
    output = ""
    for i in range(32):
        output += random.choice("0123456789abcdef")
    return output
    
def rescramble():
    '''Handle a Re-scramble. Clean up data, and reset XFCE session'''
    nm_files= []
    flag_file="/tmp/rescramble"
    try:
        nm_files = glob.glob("/var/lib/NetworkManager/internal-*.lease")
        for file in nm_files:
            os.remove(file)
    except:
        warn("Could not remove all NetworkManager lease file")
    # Get a list of all connections in Network Manager
    con_list = bytes2string( subprocess.check_output("nmcli -t con",shell=True) )
    con_list = con_list.split('\\n')
    line = []
    # Delete them all
    for con in con_list:
        line = con.split(":")
        try:
            subprocess.call("nmcli con delete uuid " + line[1],shell=True)
        except:
            continue
    try:
        subprocess.call("systemctl restart NetworkManager",shell=True)
    except:
        warn("Could not restart NetworkManager")

    # Touch /tmp/rescramble to let the OS know there is a new file
    iface_list = get_eth_interfaces()
    iface_list = " ".join(iface_list)
    temp_file = open(flag_file,"w")
    temp_file.write(iface_list)
    temp_file.close
    os.chown(flag_file,1000,985)
    os.chmod(flag_file,0o660)

    try:
        subprocess.call("pkill X",shell=True)
    except:
        exit_with_error(1,"Screen Reset failed!")

def message(message):
    print(colors.bold + "mh_scramble.sh: " + colors.reset + message)

def submsg(message):
    print("\t       " + message)

def exit_with_error(exit,message):
    print(colors.bold + "mh_scramble.sh: " + colors.red +" ¡ERROR!: " + colors.reset + message, file=sys.stderr)
    sys.exit(exit)

def warn(message):
    print(colors.bold + "mh_scramble.sh: " + colors.yellow +"¡WARN!: " + colors.reset + message, file=sys.stderr)
    return

def main():
    '''main program'''
    #start with the argument parser
    parser = argparse.ArgumentParser(description='''MAC and HOST scramble: Scrambles Ethernet MAC addresses, system hostname, and /etc/machine-id''')
    parser.add_argument('-s','--rescramble',help="Regenerate Scrambled data and reset screen",action="store_true")
    args = parser.parse_args()

    print( colors.bold + colors.cyan + "--+" + colors.lightgrey + " Mac and Host Scramble " + colors.cyan + "+--" + colors.reset)
    # modern versions of NetworkManager have a built in scrambler, if configured
    # use this instead. It will give every new connection a random MAC address
    if args.rescramble != True:
        outfile = open("/etc/NetworkManager/conf.d/10-mac-randomization.conf","w")
        outfile.write('''[device-mac-randomization]
wifi.scan-rand-mac-address=yes

[connection-mac-randomization]
ethernet.cloned-mac-address=stable
wifi.cloned-mac-address=stable
''')

    # Resets /etc/machine-id, used by udev to "identify machines on networks
    # with changing MAC addresses".
    machine_id = random_machine_id()
    os.chmod('/etc/machine-id',0o644)
    outfile = open('/etc/machine-id',"w")
    outfile.write(machine_id)
    outfile.write("\n")
    outfile.close()
    os.chmod('/etc/machine-id',0o444)

    #new hostname, we still use APG for pronouncable passwords
    new_hostname = random_hostname()
    #write it to file so it works
    try:
        subprocess.check_call("hostnamectl set-hostname " + new_hostname,shell=True)
    except:
        warn("Cannot set hostname, root?")
    #write /etc/hosts
    try:
        infile  = open("/etc/hosts.head","r")
        hosts_header = infile.read()
        infile.close()
        
        hosts_text   = '127.0.0.1        localhost.localdomain    localhost ' + new_hostname + '\n'
        hosts_text  += '::1              localhost.localdomain    localhost ' + new_hostname + '\n'
        
        outfile = open("/etc/hosts","w")
        outfile.write(hosts_header+"\n")
        outfile.write(hosts_text)
        outfile.close()
    except:
        warn("Cannot write to /etc/hosts, you need to manually add the new hostname to 127.0.0.1 in /etc/hosts")

    #Now print message and exit.
    exit_string= "changed hostname to " + colors.bold + new_hostname + colors.reset + ". NetworkManger config adjusted for random macs. Scrambled /etc/machine-id. Lasts until reboot"
    message(exit_string)
    
    # if this script was called with --rescramble,
    # lets kill X which will log the system out, which will re-trigger the
    # autologin which is necessary because XFCE freaks out and stops working if
    # the system hostname changes while running.
    if args.rescramble == True:
        rescramble()

if __name__ == "__main__":
    main()

