#!/usr/bin/env bash
#
#  Written for the NinjaOS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# Based on I2P_control.sh, same directory. Replacement for vidalia, now that it
# has been long abandoned and now Qt4 is dropped.

ACTION=${1,,}
#TOR_CONSOLE=""
declare -i EXIT=0

notify_status(){
    #returns active for working, inactive for not working
    local status="$(systemctl is-active tor)"
    case $status in
      active)
        notify-send "Onion Router: Running" "You are connecting/connected to The Onion Router" --icon=tor-start
        echo "status: enabled"
        return 0
        ;;
      inactive|unknown)
        notify-send "Onion Router: Stopped!" "You are not connected to The Onion Router" --icon=tor-stop
        echo "status: disabled"
        return 0
        ;;
      *)
        notify-send "Onion Router: Unexpected ERROR" "Something has gone terribly wrong, please check tor and systemctl manually" --icon=tor-error
        echo "status: systemd_error"
        return 1
        ;;
    esac
}

help_and_exit() {
    echo "$(tput bold)tor_control.sh:$(tput sgr0)" 1>&2
    cat 1>&2 << EOF
This script is a freedesktop menu wrapper for starting and stopping the
onion router service, as well as checking status.

	USAGE:
tor_control.sh [start|stop|status]

EOF
exit 1
}

case $ACTION in
    start)
      pkexec systemctl start tor
      EXIT=$?
      notify-send "Starting TOR" "Connecting to The Onion Router" --icon=tor-start
      echo "starting"
      ;;
    stop)
      pkexec systemctl stop tor
      EXIT=$?
      notify-send "Stoping TOR" "Disconnected from The Onion Router" --icon=tor-stop
      echo "stopping"
      ;;
    status)
      notify_status
      EXIT=$?
      ;;
    console)
      #xdg-open "$TOR_CONSOLE"
      echo "No_Console"
      notify-send "Onion Router" "No Console Defined!"
      EXIT=$?
      ;;
    *)
      help_and_exit
      ;;
esac

if [[ $EXIT -ne 0 ]];then
    echo "tor_control.sh: Script threw a code, previous action failed" 1>&2
    notify-send "Onion Router: FAIL!" "Last Action Failed, exit code: $EXIT" --icon=tor-error
fi
exit ${EXIT}
