#!/bin/bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# BOOT PARTITION VARIABLES

boot_help(){
  cat 1>&2 << EOF
livesos_boot.sh : This is a bash library for getting the physical parition name
that Ninja OS was booted from. This is likely going to be larch specific. It
is unknown of the port to bdisk will change this. If \$BOOTPART or \$BOOTDEV are
not present and block devices, the script returns an error code of 1.

	VARIABLES:

	\$BOOTPART	- Variable with the partition that /boot is mounted on
			This is always the physical media.

	\$BOOTDEV	- the physical device which contains the Live OS.

	\$CMDLINE	- the contents of /proc/cmdline. contains all kernel
			variables

EOF
}

#parition that /boot is mounted on. this is larch specific.
BOOTPART=$(cat /proc/mounts |grep "/.livesys/medium ext4" |cut -d " " -f 1)
# assume /boot is mounted on /dev/sda1. This bit of happy horse shit below is
# what you need to get /dev/sda from /dev/sda1 in bash. So how does that work?
# pay attention and you notice it uses fairly routine bash slicing
# ${varname:start:stop}. Except the stop uses bash math $(( )) to count the
# length in letters ${#varname} subtracted by one. This strips off the last
# character by matching everything everything except the last character.
BOOTDEV=${BOOTPART:0:$((${#BOOTPART}-1))}

#contents of the kernel command line, i.e. from the boot loader
CMDLINE=$(cat /proc/cmdline)


# If $BOOTPART or $BOOTDEV do not exist as block devices return value is 1
[ -b ${BOOTPART} ] || exit 1
[ -b ${BOOTDEV}  ] || exit 1

