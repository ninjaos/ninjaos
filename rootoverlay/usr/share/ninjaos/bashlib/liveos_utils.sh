#!/bin/bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# bash library for Utility functions for Ninja OS

utils_help(){
  cat 1>&2 << EOF
liveos_utils.sh:

This is a bash library written for Ninja OS that contains the following:

	FUNCTIONS:
	check_sudo	- check if we can obtain root with sudo

	parse_config	- less dangerously source variables from a config file.
			Imports KEY=pair values as UPPERCASE named variables.
			Can use as a dirrect replacement for source

	secure_parse_config - saves all KEY=pair value items to an array named
			      $CONFIG. This is "secure" because it does not
			      overwrite variables in the script.

	check_deps	- checks for presence of programs in the DEP_LIST=""
			variable returns 0 for success, 1 for failure. failure
			gives a msg

	panic_reboot	- reboot implemented entirely in shell using sysrq.
EOF
  exit 2
}

check_sudo() {
  # test should equal "root"
  local test=""
  test=$(sudo whoami 2> /dev/null )
  if [ ${test} == "root" ];then
    echo "TRUE"
   else
    echo "FALSE"
  fi
}

parse_environment(){
  # parse a key=pair shell enviroment file. NOTE all keys will be made UPPERCASE
  # variables. in parent script.

  local infile="${@}"
  local safe_config=$(mktemp)
  local key=""
  local value=""
  
  [ -f "${infile}" ] || return 2 # infile is not a file
  # Now we have an array of file lines
  readarray file_lines < "${infile}" || return 1 # error proccessing

  for line in "${file_lines[@]}";do
    # Remove comments
    [[ -z "{$line}" || "${line}" == "#" ]] && continue
    line=$(cut -d "#" -f 1 <<< ${line} )

    # Split key and value from lines
    key=$(cut -d "=" -f 1 <<< ${line} )
    value=$(cut -d "=" -f 2 <<< ${line} )

    # Parse key. Make the Key uppercase, remove spaces and all non-alphanumeric
    # characters
    key="${key^^}"
    key="${key// /}"
    key="$(tr -cd "[:alnum:]" <<< $key)"

    # Parse value. Remove anything that can escape a variable and run code.
    value="$(tr -d ";|&()" <<< $value )"

    # Zero check. If after cleaning either the key or value is null, then
    # write nothing
    [ -z "${key}" ] && continue
    [ -z "${value}" ] && continue

    # write sanitized values to temp file
    echo "${key}=${value}" >> ${safe_config}
  done

  #Now, we can import the cleaned config and then delete it.
  source ${safe_config}
  rm -f ${safe_config}
}

################################################################################
parse_config(){
  # parse a key=pair configuration file into an array called CONFIG.
  declare -A CONFIG
  local infile="${@}"
  local key=""
  local value=""
  local -a file_lines
  local line=""

  [ -f ${infile} ] || return 2 # infile is not a file
  # Now we have an array of file lines
  readarray file_lines < "${infile}" || return 1 # error proccessing

  for line in "${file_lines[@]}";do
    # Remove comments
    [[ -z "${line}" || "${line}" == "#" ]] && continue
    line="$(cut -d "#" -f 1 <<< ${line} )"

    # Split key and value from lines
    key="$(cut -d "=" -f 1 <<< ${line} )"
    value="$(cut -d "=" -f 2 <<< ${line} )"

    # Parse key. Alphanumeric keys only
    key="${key// /}"
    key="$(tr -cd "[:alnum:]" <<< $key)"
    # Parse value. Remove anything that can escape a variable and run code.
    value="$(tr -d ";|&" <<< $value )"
 
    # Zero check. If after cleaning either the key or value is null, then
    # do nothing
    [ -z "${key}" ] && continue
    [ -z "${value}" ] && continue

    # Enter sanitized values to array "CONFIG"
    CONFIG["${key}"]="${value}"
  done
}

check_deps(){
  #This function checks dependencies. looks for executable binaries in path
  # returns 1 if a dep is not found, and 0 if all deps are present
  for dep in ${DEP_LIST};do
    which ${dep} &> /dev/null
    if [ $? -ne 0 ];then
      echo "check_dep_fail ${dep}"
      return 1
    fi
  done
  return 0
}

panic_reboot(){
  # Enable sysrq in proc
  echo 1 > /proc/sys/kernel/sysrq
  # reboot command
  echo b > /proc/sysrq-trigger
}

