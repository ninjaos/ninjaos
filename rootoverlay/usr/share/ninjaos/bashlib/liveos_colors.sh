#!/bin/bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#  This library has terminal color variables for Ninja OS

colors_help(){
  cat 1>&2 << EOF
liveos_colors.sh : This is a library for terminal color variables written for
Ninja OS. Using a variable will change the color until a new color is selected

	Colors:
	${BRIGHT_RED}\$BRIGHT_RED		${BRIGHT_GREEN}\$BRIGHT_GREEN
	${BRIGHT_YELLOW}\$BRIGHT_YELLOW		${BRIGHT_CYAN}\$BRIGHT_CYAN
	${GREY}\$GREY			${BRIGHT_WHITE}\$BRIGHT_WHITE

${NOCOLOR}\$NOCOLOR - resets color to default

EOF
}
#pretty terminal colors
BRIGHT_RED=$(tput setaf 1;tput bold)
BRIGHT_GREEN=$(tput setaf 2;tput bold)
BRIGHT_YELLOW=$(tput setaf 3;tput bold)
BRIGHT_CYAN=$(tput setaf 6;tput bold)
GREY=$(tput bold;tput setaf 0)
BRIGHT_WHITE=$(tput bold;tput setaf 7)
BRIGHT=$(tput bold)
NOCOLOR=$(tput sgr0)

