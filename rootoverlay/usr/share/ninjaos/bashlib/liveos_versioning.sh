#!/bin/bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#  this is a bash library with the versioning functions and vars for Ninja OS
#  Also included is the GPG stuff

versioning_help(){
  cat 1>&2 << EOF
liveos_version.sh : bash library that contains variables and functions for
for checking GPG and versioning info for Ninja OS. versioning information is
stored in /var/liveos_version.conf. exit codes 1 for parse error, 2 for verison
file not found

	VARIABLES:
	\$VERSION_FILE	- Version file versioning information is read from.
			This defaults to /var/liveos_version

	\$FORMAT_VER	- Revision version of the version file. Version 1
			started with Ninja OS 0.11.0. verison 1.1 Ninja OS
			0.11.1 and Revision 2 in Ninja OS 0.11.2

	\$OSNAME		- Name of the Operating System

	\$OSSLUG		- UNIX friendly(no spaces, all lowercase) name
			of the OS

	\$OSARCH		- Machine Archecture of the OS, i.e. i686, x86_64

	\$PART_SIZE	- Size of Operating System partition in MegaBytes.
			995 - up to version 0.8
			1225 - up to version 0.10
			1998 - verison 0.11 to current

	\$BOOT_BACKGROUND - JPG file used as background for syslinux

	\$BACKGROUND_SHA256 - sha256 hash sume of \$BOOT_BACKGROUND

	\$GPG_KEYRING	- file with a GPG keyring that contains the public key
			of the OS. with Ninja OS, this keyring contains only
			that key.

	\$GPG_FINGERPRINT - Fingerprint of the key from \$GPG_KEYRING

	\$GPG_KEYNAME	- *DEPRECIATED*. Last 8 of GPG_FINGERPRINT
    
   	\$CONF_KEYSIG	- Fingerpring of the key from $\VERSION_FILE

	\$CONF_KEYNAME	- *DEPRECIATED*. Last 8 of CONF_KEYSIG



	FUNCTIONS:
	gpg_key_check - check that the public key in the keyring is the same
			as the one in the liveos_version.conf checks
			\$CONF_KEYSIG against \$GPG_FINGERPRING and \$GPG_KEYNAME
			against \$CONF_KETYNAME

	read_old_version_file - reads versioning file from the Ninja OS 0.10 and
				before using LIVEOS_VERSION.TXT. Adds the
				following variables:
 	\$OLD_OSNAME		\$OLD_OSVERSION		\$OLD_BACKGROUND
	\$OLD_BACKGROUND_SHA256				\$OLD_VERSION_FILE

EOF
}

VERSION_FILE="/var/liveos_version.conf"
#[ -f "${PWD}/liveos_version.conf" ] && VERSION_FILE="${PWD}/liveos_version.conf"
[ -f "${VERSION_FILE}" ] || exit 2

source "${VERSION_FILE}" || exit 1

# the slug is a sanitized for unix name space version of name. lowercase and no
# spaces
OSSLUG=${OSNAME,,}
OSSLUG=${OSSLUG//[[:blank:]]/}

#GPG stuff.
LOCAL_GPG_KEYRING="/usr/share/ninjaos/ninja_pubring.gpg"
PACK_GPG_KEYRING="./gpg/package_key.gpg"
GPG_KEYRING="${LOCAL_GPG_KEYRING}"
#GPG_KEYNAME=$(gpg --no-default-keyring --keyring "${GPG_KEYRING}" --fingerprint| awk 'NR==4{print $9$10}')
GPG_FINGERPRINT=$(gpg --no-default-keyring --keyring "${GPG_KEYRING}" --list-keys |awk 'NR==4{print $1}')
GPG_KEYNAME=${GPG_FINGERPRINT: -8}

gpg_check_key() {
  if [[ ${CONF_KEYSIG} == ${GPG_FINGERPRINT} ]];then
    echo "TRUE"
   else
    echo "FALSE"
  fi
}

read_old_version_file(){
  # Read the pre 0.11.x LIVEOS_VERSION.TXT
  # If a parameter is present it is the old config
  local version_file="${@}"
  [ -f "${@}/LIVEOS_VERSION.TXT" ] || return 2
  OLD_OSNAME=$(awk 'NR==1{print $1}' "${version_file}")
  OLD_OSVERSION=$(awk 'NR==1{print $2}' "${version_file}")
  OLD_BACKGROUND=$(awk 'NR==2{print $1}' "${version_file}")
  OLD_BACKGROUND_SHA256=$(awk 'NR==2{print $2}' "${version_file}")
  OLD_LINE5=$(awk 'NR==5' "${version_file}")
  OLD_VERSION_FILE=${version_file}
}

