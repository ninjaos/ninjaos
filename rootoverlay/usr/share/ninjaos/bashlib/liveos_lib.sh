#!/bin/bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
# Bash Library to use common LiveOS $VARIABLES in scripts.
# add ". /usr/share/ninjaos/scripts/liveos_lib.sh" to the top of your script.
# This is legacy. functionality split into other files in this directory

versioning_help(){
  cat 1>&2 << EOF
liveos_version.sh : bash library that contains variables and functions for
for checking GPG and versioning info for Ninja OS. versioning information is
stored in /var/liveos_version.conf. exit codes 1 for parse error, 2 for verison
file not found

	VARIABLES:
	\$VERSION_FILE	- Version file versioning information is read from.
			This defaults to /var/liveos_version

	\$FORMAT_VER	- Revision version of the version file. Version 1
			started with Ninja OS 0.11.0. verison 1.1 Ninja OS
			0.11.1 and Revision 2 in Ninja OS 0.11.2

	\$OSNAME		- Name of the Operating System

	\$OSSLUG		- UNIX friendly(no spaces, all lowercase) name
			of the OS

	\$OSARCH		- Machine Archecture of the OS, i.e. i686, x86_64

	\$PART_SIZE	- Size of Operating System partition in MegaBytes.
			995 - up to version 0.8
			1225 - up to version 0.10
			1998 - verison 0.11 to current

	\$BOOT_BACKGROUND - JPG file used as background for syslinux

	\$BACKGROUND_SHA256 - sha256 hash sume of \$BOOT_BACKGROUND

	\$GPG_KEYRING	- file with a GPG keyring that contains the public key
			of the OS. with Ninja OS, this keyring contains only
			that key.

	\$GPG_FINGERPRINT - Fingerprint of the key from \$GPG_KEYRING

	\$GPG_KEYNAME	- *DEPRECIATED*. Last 8 of GPG_FINGERPRINT
    
   	\$CONF_KEYSIG	- Fingerpring of the key from $\VERSION_FILE

	\$CONF_KEYNAME	- *DEPRECIATED*. Last 8 of CONF_KEYSIG



	FUNCTIONS:
	gpg_key_check - check that the public key in the keyring is the same
			as the one in the liveos_version.conf checks
			\$CONF_KEYSIG against \$GPG_FINGERPRING and \$GPG_KEYNAME
			against \$CONF_KETYNAME

	read_old_version_file - reads versioning file from the Ninja OS 0.10 and
				before using LIVEOS_VERSION.TXT. Adds the
				following variables:
 	\$OLD_OSNAME		\$OLD_OSVERSION		\$OLD_BACKGROUND
	\$OLD_BACKGROUND_SHA256				\$OLD_VERSION_FILE

EOF
}

VERSION_FILE="/var/liveos_version.conf"
#[ -f "${PWD}/liveos_version.conf" ] && VERSION_FILE="${PWD}/liveos_version.conf"
[ -f "${VERSION_FILE}" ] || exit 2

source "${VERSION_FILE}" || exit 1

# the slug is a sanitized for unix name space version of name. lowercase and no
# spaces
OSSLUG=${OSNAME,,}
OSSLUG=${OSSLUG//[[:blank:]]/}

#GPG stuff.
LOCAL_GPG_KEYRING="/usr/share/ninjaos/package_key.gpg"
PACK_GPG_KEYRING="./gpg/package_key.gpg"
GPG_KEYRING="${LOCAL_GPG_KEYRING}"
#GPG_KEYNAME=$(gpg --no-default-keyring --keyring "${GPG_KEYRING}" --fingerprint| awk 'NR==4{print $9$10}')
GPG_FINGERPRINT=$(gpg --no-default-keyring --keyring "${GPG_KEYRING}" --list-keys |awk 'NR==4{print $1}')
GPG_KEYNAME=${GPG_FINGERPRINT: -8}

gpg_check_key() {
  if [[ ${CONF_KEYSIG} == ${GPG_FINGERPRINT} ]];then
    echo "TRUE"
   else
    echo "FALSE"
  fi
}

read_old_version_file(){
  # Read the pre 0.11.x LIVEOS_VERSION.TXT
  # If a parameter is present it is the old config
  local version_file="${@}"
  [ -f "${@}/LIVEOS_VERSION.TXT" ] || return 2
  OLD_OSNAME=$(awk 'NR==1{print $1}' "${version_file}")
  OLD_OSVERSION=$(awk 'NR==1{print $2}' "${version_file}")
  OLD_BACKGROUND=$(awk 'NR==2{print $1}' "${version_file}")
  OLD_BACKGROUND_SHA256=$(awk 'NR==2{print $2}' "${version_file}")
  OLD_LINE5=$(awk 'NR==5' "${version_file}")
  OLD_VERSION_FILE=${version_file}
}


boot_help(){
  cat 1>&2 << EOF
livesos_boot.sh : This is a bash library for getting the physical parition name
that Ninja OS was booted from. This is likely going to be larch specific. It
is unknown of the port to bdisk will change this. If \$BOOTPART or \$BOOTDEV are
not present and block devices, the script returns an error code of 1.

	VARIABLES:

	\$BOOTPART	- Variable with the partition that /boot is mounted on
			This is always the physical media.

	\$BOOTDEV	- the physical device which contains the Live OS.

	\$CMDLINE	- the contents of /proc/cmdline. contains all kernel
			variables

EOF
}

#parition that /boot is mounted on. this is larch specific.
BOOTPART=$(cat /proc/mounts |grep "/.livesys/medium ext4" |cut -d " " -f 1)
# assume /boot is mounted on /dev/sda1. This bit of happy horse shit below is
# what you need to get /dev/sda from /dev/sda1 in bash. So how does that work?
# pay attention and you notice it uses fairly routine bash slicing
# ${varname:start:stop}. Except the stop uses bash math $(( )) to count the
# length in letters ${#varname} subtracted by one. This strips off the last
# character by matching everything everything except the last character.
BOOTDEV=${BOOTPART:0:$((${#BOOTPART}-1))}

#contents of the kernel command line, i.e. from the boot loader
CMDLINE=$(cat /proc/cmdline)


# If $BOOTPART or $BOOTDEV do not exist as block devices return value is 1
[ -b ${BOOTPART} ] || exit 1
[ -b ${BOOTDEV}  ] || exit 1

#!/bin/bash
#
#  Written for Ninja OS by the development team.
#  licensed under the GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
#
#  This library has terminal color variables for Ninja OS

colors_help(){
  cat 1>&2 << EOF
liveos_colors.sh : This is a library for terminal color variables written for
Ninja OS. Using a variable will change the color until a new color is selected

	Colors:
	${BRIGHT_RED}\$BRIGHT_RED		${BRIGHT_GREEN}\$BRIGHT_GREEN
	${BRIGHT_YELLOW}\$BRIGHT_YELLOW		${BRIGHT_CYAN}\$BRIGHT_CYAN
	${GREY}\$GREY			${BRIGHT_WHITE}\$BRIGHT_WHITE

${NOCOLOR}\$NOCOLOR - resets color to default

EOF
}
#pretty terminal colors
BRIGHT_RED=$(tput setaf 1;tput bold)
BRIGHT_GREEN=$(tput setaf 2;tput bold)
BRIGHT_YELLOW=$(tput setaf 3;tput bold)
BRIGHT_CYAN=$(tput setaf 6;tput bold)
GREY=$(tput bold;tput setaf 0)
BRIGHT_WHITE=$(tput bold;tput setaf 7)
BRIGHT=$(tput bold)
NOCOLOR=$(tput sgr0)


utils_help(){
  cat 1>&2 << EOF
liveos_utils.sh:

This is a bash library written for Ninja OS that contains the following:

	FUNCTIONS:
	check_sudo	- check if we can obtain root with sudo

	parse_config	- less dangerously source variables from a config file.
			Imports KEY=pair values as UPPERCASE named variables.
			Can use as a dirrect replacement for source

	secure_parse_config - saves all KEY=pair value items to an array named
			      $CONFIG. This is "secure" because it does not
			      overwrite variables in the script.

	check_deps	- checks for presence of programs in the DEP_LIST=""
			variable returns 0 for success, 1 for failure. failure
			gives a msg

	panic_reboot	- reboot implemented entirely in shell using sysrq.
EOF
  exit 2
}

check_sudo() {
  # test should equal "root"
  local test=""
  test=$(sudo whoami 2> /dev/null )
  if [ ${test} == "root" ];then
    echo "TRUE"
   else
    echo "FALSE"
  fi
}

parse_environment(){
  # parse a key=pair shell enviroment file. NOTE all keys will be made UPPERCASE
  # variables. in parent script.

  local infile="${@}"
  local safe_config=$(mktemp)
  local key=""
  local value=""
  
  [ -f "${infile}" ] || return 2 # infile is not a file
  # Now we have an array of file lines
  readarray file_lines < "${infile}" || return 1 # error proccessing

  for line in "${file_lines[@]}";do
    # Remove comments
    [[ -z "{$line}" || "${line}" == "#" ]] && continue
    line=$(cut -d "#" -f 1 <<< ${line} )

    # Split key and value from lines
    key=$(cut -d "=" -f 1 <<< ${line} )
    value=$(cut -d "=" -f 2 <<< ${line} )

    # Parse key. Make the Key uppercase, remove spaces and all non-alphanumeric
    # characters
    key="${key^^}"
    key="${key// /}"
    key="$(tr -cd "[:alnum:]" <<< $key)"

    # Parse value. Remove anything that can escape a variable and run code.
    value="$(tr -d ";|&()" <<< $value )"

    # Zero check. If after cleaning either the key or value is null, then
    # write nothing
    [ -z "${key}" ] && continue
    [ -z "${value}" ] && continue

    # write sanitized values to temp file
    echo "${key}=${value}" >> ${safe_config}
  done

  #Now, we can import the cleaned config and then delete it.
  source ${safe_config}
  rm -f ${safe_config}
}

################################################################################
parse_config(){
  # parse a key=pair configuration file into an array called CONFIG.
  declare -A CONFIG
  local infile="${@}"
  local key=""
  local value=""
  local -a file_lines
  local line=""

  [ -f ${infile} ] || return 2 # infile is not a file
  # Now we have an array of file lines
  readarray file_lines < "${infile}" || return 1 # error proccessing

  for line in "${file_lines[@]}";do
    # Remove comments
    [[ -z "${line}" || "${line}" == "#" ]] && continue
    line="$(cut -d "#" -f 1 <<< ${line} )"

    # Split key and value from lines
    key="$(cut -d "=" -f 1 <<< ${line} )"
    value="$(cut -d "=" -f 2 <<< ${line} )"

    # Parse key. Alphanumeric keys only
    key="${key// /}"
    key="$(tr -cd "[:alnum:]" <<< $key)"
    # Parse value. Remove anything that can escape a variable and run code.
    value="$(tr -d ";|&" <<< $value )"
 
    # Zero check. If after cleaning either the key or value is null, then
    # do nothing
    [ -z "${key}" ] && continue
    [ -z "${value}" ] && continue

    # Enter sanitized values to array "CONFIG"
    CONFIG["${key}"]="${value}"
  done
}

check_deps(){
  #This function checks dependencies. looks for executable binaries in path
  # returns 1 if a dep is not found, and 0 if all deps are present
  for dep in ${DEP_LIST};do
    which ${dep} &> /dev/null
    if [ $? -ne 0 ];then
      echo "check_dep_fail ${dep}"
      return 1
    fi
  done
  return 0
}

panic_reboot(){
  # Enable sysrq in proc
  echo 1 > /proc/sys/kernel/sysrq
  # reboot command
  echo b > /proc/sysrq-trigger
}
